<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="${base}/scripts/jquery.min.js"></script>
    <script src="${base}/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="${base}/styles/semantic.min.css"/>

    <title><@block name="title"></@block></title>

</head>
<body>


<div class="ui grid ">
    <div class="row">
        <img class="ui  image  centered medium" src="${base}/template/image/title.jpg" title="" style="height: 150px">
    </div>
</div>

<table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="three wide "></td>
            <td class="ten wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>

                        <tr>
                            <td class="two wide center aligned "><b>姓名</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="name" placeholder="请输入姓名">
                                </div>
                            </td>
                            <td class="two wide center aligned "><b>密码</b></td>
                            <td class="four wide  center aligned ">
                                <div class="field">
                                    <input type="text" name="password" placeholder="请输入密码">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide center aligned "><b>性别</b></td>
                            <td class="four wide  center aligned">
                                <div class="field ">
                                    <select class="ui dropdown  " name="sex">
                                        <option>选择性别</option>
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>
                            </td>
                            <td class="two wide center aligned"><b>电话</b></td>
                            <td class="four wide center aligned">
                                <div class="field">
                                    <input type="text" name="tel" placeholder="请输入电话">
                                </div>
                            </td>
                        </tr>
                        <tr>

                            <td class="two wide center aligned"><b>邮件</b></td>
                            <td class="four wide center aligned">
                                <div class="field">
                                    <input type="text" name="email" placeholder="请输入邮件">
                                </div>
                            </td>
                            <td class="two wide center aligned "><b>地址</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="address" placeholder="请输入地址">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <div class="ui error message"></div>
                            </td>
                            <td>
                                <div class="ui fluid large teal submit button ">注册</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </td>
            <td class="three wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '电话信息不能为空'
                    }]
                },
                name: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '姓名不能为空'
                        }]
                },
                address: {
                    rules: [{
                        type: 'empty',
                        prompt: '地址不能为空'
                    }]
                },
                sex: {
                    rules: [{
                        type: 'empty',
                        prompt: '性别未选择'
                    }]
                },
                password: {
                    rules: [{
                        type: 'empty',
                        prompt: '密码不能为空'
                    }]
                },
                email: {
                    rules: [{
                        type: 'empty',
                        prompt: '邮件不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/doRegisterCustomer',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/login'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>


</body>

<script>
    $('.ui.dropdown').dropdown();
</script>

</html>