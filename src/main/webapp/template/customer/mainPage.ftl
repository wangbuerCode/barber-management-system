
<@override name="title">首页</@override>
<@override name="content">



	<div class="ui hidden divider"></div>
	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<div class="ui raised very padded text container segment">
				<h2 class="ui header">欢迎使用美发工作室系统</h2>
<p>
	随着人们生活水平的提高，对美发的需求也逐步扩大，美发工作室的业务量大大增加。传统的人工管理方式已不能满足业务量的需求。美发工作室信息管理系统旨在将客户、美发师、美发项目、预约、工时统计等信息进行电子化管理与统计分析，提高管理效率和减轻管理员工作负担的同时对工作室业务发展提供信息支持。本课题主要以Mysql为数据库，使用Java和JavaScript开发一个实用高效的美发工作室管理系统，实现美发全过程的电子化管理和统计分析
</p>
			</div>
		</div>
		<div class=" three wide column">
		</div>
	</div>
</@override>
<@extends name="customerLayout.ftl"></@extends>