<#-- @ftlvariable name="recruit" type="model.Recruit" -->

<@override name="title">预约</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="four wide "></td>
            <td class="eight wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>
                        <tr>
                            <td class="four wide center aligned "><b>理发类型</b></td>
                            <td class="six wide  center aligned">
                                <div class="field">
                                    <input type="text" name="type" placeholder="请输入">
                                </div>
                            </td>
                            <td>
                                <div class="ui fluid large teal submit button ">预约</div>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </form>
            </td>
            <td class="four wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                name: {
                    rules: [{
                        type: 'empty',
                        prompt: '类型信息不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/customer/doAppointmentHaircut',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/customer/informationSearch'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="customerLayout.ftl"></@extends>