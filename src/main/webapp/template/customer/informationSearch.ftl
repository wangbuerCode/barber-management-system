<@override name="title">历史记录</@override>
<@override name="content">

	<table class="ui table celled padded  ">
		<tbody>
		<tr>
			<td class="two wide "></td>
			<td class="eleven wide ">
				<table class="ui red table equal width celled padded ">
					<thead>
					<tr>
						<th class=" center aligned  " colspan="5">
							搜索
						</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<form class="ui fluid form">
								<table class="ui celled table">
									<tbody>
									<tr>
										<td>
											<div class="filed">
												<div class="ui labeled input">
													<div class="ui label">
														美发项目
													</div>
													<select class="ui dropdown  " name="type">
														<option value="">选择美发项目</option>
														<option value="染发">染发</option>
														<option value="普通理发">普通理发</option>
														<option value="烫发">烫发</option>
														<option value="保养">保养</option>
														<option value="脏辫">脏辫</option>
														<option value="光头">光头</option>
													</select>
												</div>
											</div>
										</td>
										<td>
											<div class="filed">
												<div class="ui labeled input ">
													<div class="ui label">
														理发师
													</div>
													<select class="ui dropdown  " name="staffId">
														<option value="">选择理发师</option>
														<#list staff as staff>
															<option value="${staff.id}">${staff.name}</option>
														</#list>
													</select>
												</div>
											</div>
										</td>
										<td class="center aligned">
											<a class="ui primary submit button ">搜索</a><br>
										</td>
									</tr>
									<tr>
										<div class="ui error message"></div>
									</tr>

									</tbody>
								</table>
							</form>
						</td>
					</tr>
					<tr>
						<td>
							<table class="ui celled table">
								<thead>
								<tr>
									<th class="center aligned">记录序号</th>
									<th class="center aligned">理发师姓名</th>
									<th class="center aligned">理发时间</th>
									<th class="center aligned">理发类型</th>
								</tr>
								</thead>
								<tbody>
								<#if  !list.isEmpty()  >
									<#list   list as list>
										<tr>
											<td class="center aligned">
												${list.id}
											</td>
											<td class="center aligned">
												${list.staffName}
											</td>
											<td class="center aligned">
												${list.date}
											</td>
											<td class="center aligned">
												${list.type}
											</td>
										</tr>


									</#list>
									<tr>
										<td colspan="2"></td>
										<td colspan="2">
											<div class="ui positive  message">
												<div class="header center aligned">
													总共查询到${count}条记录
												</div>
											</div>
										</td>
									</tr>
								<#else >

									<tr>
										<td class="fluid">
											<div class="ui negative massive message">
												<div class="header">
													未查询到“已完成理发”的相关信息
												</div>
											</div>
										</td>
									</tr>

								</#if>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>

				</table>
			</td>

			<td class="two wide"></td>
		</tr>
		</tbody>
	</table>




	<script>
        $('.ui.form').form({
            fields: {}
        }).api({
            method: 'POST',
            url: '${base}/customer/queryHistory',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    window.location.href = '${base}/customer/queryHistoryResult'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        });
	</script>

</@override>
<@extends name="customerLayout.ftl"></@extends>