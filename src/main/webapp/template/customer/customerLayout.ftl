<!doctype html>
<html lang="zh">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
	      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<script src="${base}/scripts/jquery.min.js"></script>
	<script src="${base}/scripts/semantic.min.js"></script>
	<link rel="stylesheet" href="${base}/styles/semantic.min.css"/>

	<title><@block name="title"></@block></title>

</head>
<body>


<div class="ui grid ">
	<div class="row">
		<img class="ui  image  centered medium" src="${base}/template/image/title.jpg" title="" style="height: 150px">
	</div>
</div>

<div class="ui attached stackable menu">
	<div class="ui container">
		<div class="header item">
	美发工作室
		</div>


		<a class="ui  item" href="${base}/customer/appointmentHaircut">预约理发</a>
		<a class="ui  item" href="${base}/customer/informationSearch">信息查询</a>

		<div class="right menu">


            <#if (session.user)??>
				<div class="ui dropdown item">
					<i class="user icon"></i>
					<div class="text"> 欢迎 ${session.user.name} 用户</div>
					<i class="icon dropdown"></i>
					<div class="menu">
						<a href="${base}/logout" class="item">退出</a>
					</div>
				</div>
            <#else>

				<div class="ui buttons">
					<a class="ui button" href="${base}/login">登陆</a>
					<div class="or"></div>
					<a class="ui positive button" href="${base}/register">客户注册</a>
				</div>
            </#if>
		</div>
	</div>
</div>




<@block name="content">

</@block>

</body>

<script>
    $('.ui.dropdown').dropdown();
</script>

</html>