<@override name="title">金额</@override>
<@override name="content">



    <table class="ui table celled padded  ">
        <tbody>
        <tr>
            <td class="two wide "></td>
            <td class="eleven wide ">
                <table class="ui red table equal width celled padded ">
                    <thead>
                    </thead>
                    <tbody>
                    <tr>
                        <td>
                            <table class="ui celled table">
                                <thead>
                                <tr>
                                    <th class="center aligned">ID</th>
                                    <th class="center aligned">姓名</th>
                                    <th class="center aligned">电话</th>
                                    <th class="center aligned">余额</th>
                                    <th class="center aligned">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#if  !  page.isEmpty()  >
                                    <#list   page as record>
                                        <tr>
                                            <td class="center aligned">
                                                ${record.id}
                                            </td>
                                            <td class="center aligned">
                                                ${record.name}
                                            </td>

                                            <td class="center aligned">
                                                ${record.tel}
                                            </td>
                                            <td class="center aligned">
                                                ${record.money}
                                            </td>
                                            <td class="center aligned">
                                                <a href="${base}/manager/modifyRecharge/${record.id}"
                                                   class="ui button primary negative">消费扣减</a>
                                            </td>
                                        </tr>


                                    </#list>
                                <#else >

                                    <tr>
                                        <td class="fluid">
                                            <div class="ui negative massive message">
                                                <div class="header">
                                                    未查询到相关信息
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </#if>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>

                </table>
            </td>

            <td class="two wide"></td>
        </tr>
        </tbody>
    </table>
</@override>
<@extends name="managerLayout.ftl"></@extends>