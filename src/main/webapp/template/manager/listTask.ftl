<@override name="title">理发管理</@override>
<@override name="content">



    <table class="ui table celled padded  ">
        <tbody>
        <tr>
            <td class="two wide "></td>
            <td class="eleven wide ">
                <table class="ui red table equal width celled padded ">
                    <thead>
                    <tr>
                        <th class=" center aligned  " colspan="5">

                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
                            <table class="ui celled table">
                                <thead>
                                <tr>
                                    <th class="center aligned">客户ID</th>
                                    <th class="center aligned">客户姓名</th>
                                    <th class="center aligned">分配员工</th>
                                    <th class="center aligned">理发类型</th>
                                    <th class="center aligned">理发状态</th>
                                    <th class="center aligned">操作</th>

                                </tr>
                                </thead>
                                <tbody>
                                <#if  !  page.getList().isEmpty()  >
                                    <#list   page.getList() as record>
                                        <tr>
                                            <td class="center aligned">
                                                ${record.customerId}
                                            </td>
                                            <td class="center aligned">
                                                ${record.customerName}
                                            </td>
                                            <td class="center aligned">
                                                <#if record.staffId==0>
                                                 尚未分配
                                                <#else >
                                                ${record.staffName}
                                                </#if>
                                            </td>
                                            <td class="center aligned">
                                                ${record.type}
                                            </td>
                                            <td class="center aligned">
                                                <#if record.taskStatus==0>
                                                    已完成
                                                <#else >
                                                    进行中
                                                </#if>
                                            </td>

                                            <td class="center aligned">
                                                <#if record.staffId==0 >
                                                    <a href="${base}/manager/assignEmployees/${record.id}"
                                                       class="ui button primary">分配员工</a>
                                                <#else >
                                                    <a href="#"
                                                       class="ui button  disabled">分配员工</a>
                                                </#if>

                                                <#if record.taskStatus!=0>
                                                    <a href="${base}/manager/finish/${record.id}"
                                                       class="ui button red ">完成理发</a>
                                                <#else >
                                                    <a href="#"
                                                       class="ui button  disabled">完成理发</a>
                                                </#if>
                                            </td>

                                        </tr>

                                    </#list>
                                <#else >

                                    <tr>
                                        <td class="fluid">
                                            <div class="ui negative massive message">
                                                <div class="header">
                                                    未查询到相关信息
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </#if>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5">
                            <div class="ui right floated pagination menu">
                                <#if page.getPageNumber() !=1 >
                                    <a class="icon item" href="?page=${page.getPageNumber()-1}">
                                        <i class="left chevron icon"></i>
                                    </a>
                                </#if>
                                <#list  1..(page.getTotalPage()) as count >
                                    <a class="item">
                                        <#if page.getPageNumber()==count>
                                            <b>${count}</b>
                                        <#else>
                                            ${count}
                                        </#if>
                                    </a>
                                </#list>
                                <#if page.getPageNumber() != page.getTotalPage() >
                                    <a class="icon item" href="?page=${page.getPageNumber()+1}">
                                        <i class="right chevron icon"></i>
                                    </a>
                                </#if>
                            </div>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </td>

            <td class="two wide"></td>
        </tr>
        </tbody>
    </table>




    <script>
        $('.ui.form').form({
            fields: {}
        }).api({
            method: 'POST',
            url: '${base}/manager/queryStaff',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    window.location.href = '${base}/manager/queryStaffResult'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        });
    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>