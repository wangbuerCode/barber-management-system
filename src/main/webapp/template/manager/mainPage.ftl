
<@override name="title">管理员首页</@override>
<@override name="content">



	<div class="ui hidden divider"></div>
	<div class="ui grid">
		<div class="three wide column"></div>
		<div class="ten wide column">
			<div class="ui raised very padded text container segment">
				<h2 class="ui header">欢迎使用美发工作室系统</h2>
				<p>1. 此系统应包括后台数据库的设计和前台开发实现</p>
				<p>	2. 实现前台客户预约理发和查看历史记录</p>
				<p>3. 实现后台管理功能，包括顾客、员工、理发类型管理、理发员工分配和金额充值等功能模块</p>

				<p>4. 开发工具：Eclipse/IntelliJ IDEA</p>
			</div>
		</div>
		<div class=" three wide column">
		</div>
	</div>
</@override>
<@extends name="managerLayout.ftl"></@extends>