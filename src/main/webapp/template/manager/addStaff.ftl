<#-- @ftlvariable name="recruit" type="model.Recruit" -->

<@override name="title">t添加员工</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="three wide "></td>
            <td class="ten wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>

                        <tr>
                            <td class="two wide center aligned "><b>姓名</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="name" placeholder="请输入姓名">
                                </div>
                            </td>
                            <td class="two wide center aligned "><b>擅长技能</b></td>
                            <td class="four wide  center aligned ">
                                <div class="field">
                                    <select class="ui dropdown  " name="skill">
                                        <option value="染发">选择擅长技能</option>
                                        <option value="染发">染发</option>
                                        <option value="普通理发">普通理发</option>
                                        <option value="烫发">烫发</option>
                                        <option value="保养">保养</option>
                                    </select>

                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td class="two wide center aligned "><b>性别</b></td>
                            <td class="four wide  center aligned">
                                <div class="field ">
                                    <select class="ui dropdown  " name="sex">
                                        <option>选择性别</option>
                                        <option value="男">男</option>
                                        <option value="女">女</option>
                                    </select>
                                </div>
                            </td>
                            <td class="two wide center aligned"><b>电话</b></td>
                            <td class="four wide center aligned">
                                <div class="field">
                                    <input type="text" name="tel" placeholder="请输入电话">
                                </div>
                            </td>
                        </tr>
                        <tr>

                            <td class="two wide center aligned"><b>邮件</b></td>
                            <td class="four wide center aligned">
                                <div class="field">
                                    <input type="text" name="email" placeholder="请输入邮件">
                                </div>
                            </td>
                            <td class="two wide center aligned "><b>地址</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="address" placeholder="请输入地址">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <div class="ui error message"></div>
                            </td>
                            <td>
                                <div class="ui fluid large teal submit button ">保存</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </td>
            <td class="three wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                tel: {
                    rules: [{
                        type: 'empty',
                        prompt: '电话信息不能为空'
                    }]
                },
                name: {
                    rules: [
                        {
                            type: 'empty',
                            prompt: '姓名不能为空'
                        }]
                },
                address: {
                    rules: [{
                        type: 'empty',
                        prompt: '地址不能为空'
                    }]
                },
                sex: {
                    rules: [{
                        type: 'empty',
                        prompt: '性别未选择'
                    }]
                },
                skill: {
                    rules: [{
                        type: 'empty',
                        prompt: '擅长技能不能为空'
                    }]
                },
                email: {
                    rules: [{
                        type: 'empty',
                        prompt: '邮件不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/manager/doAddStaff',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/manager/listStaff'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>