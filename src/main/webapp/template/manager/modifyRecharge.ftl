<#-- @ftlvariable name="recruit" type="model.Recruit" -->

<@override name="title">消费扣减</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="three wide "></td>
            <td class="ten wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>

                        <tr>
                            <td class="two wide center aligned "><b>ID</b></td>
                            <td class="four wide  center aligned disabled">
                                <div class="field">
                                    <input type="text" name="id" value="${customerId}" placeholder="${customerId}">
                                </div>
                            </td>


                            <td class="two wide center aligned "><b>消费金额</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="ConsumptionAmount"  placeholder="请输入该用户消费金额">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <div class="ui error message"></div>
                            </td>

                            <td>
                                <div class="ui fluid large teal submit button ">保存</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </td>
            <td class="three wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                ConsumptionAmount: {
                    rules: [{
                        type: 'empty',
                        prompt: '消费金额不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/manager/doModifyRecharge',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/manager/listRecharge'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>