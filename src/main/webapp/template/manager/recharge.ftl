<#-- @ftlvariable name="recruit" type="model.Recruit" -->

<@override name="title">充值金额</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="four wide "></td>
            <td class="eight wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>

                        <tr>
                            <td class="four wide center aligned "><b>客户ID</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="id" placeholder="请输入客户ID">
                                </div>
                            </td>
                        </tr>
                        <tr>
                        <td class="two wide center aligned "><b>充值金额</b></td>
                        <td class="two wide  center aligned">
                            <div class="field">
                                <input type="text" name="money" placeholder="请输入充值金额">
                            </div>
                        </td>
                        </tr>
                        <tr>

                            <td colspan="4">

                                <div class="ui fluid large teal submit button ">保存</div>
                            </td>
                        </tr>

                        </tbody>
                    </table>
                </form>
            </td>
            <td class="four wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                id: {
                    rules: [{
                        type: 'empty',
                        prompt: '客户ID不能为空'
                    }]
                }, money: {
                    rules: [{
                        type: 'empty',
                        prompt: '金额不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/manager/doAddRecharge',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/manager/listRecharge'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>