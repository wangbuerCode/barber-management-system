<@override name="title">理发类型</@override>
<@override name="content">



    <table class="ui table celled padded  ">
        <tbody>
        <tr>
            <td class="two wide "></td>
            <td class="eleven wide ">
                <table class="ui red table equal width celled padded ">
                    <thead>
                    <tr>
                        <th class=" center aligned  " colspan="3">
                            理发类型
                        </th>
                    </tr>
                    </thead>
                    <tbody>

                    <tr>
                        <td>
                            <table class="ui celled table">
                                <thead>
                                <tr>
                                    <th class="center aligned">ID</th>
                                    <th class="center aligned">类型</th>
                                    <th class="center aligned">操作</th>
                                </tr>
                                </thead>
                                <tbody>
                                <#if  !  page.getList().isEmpty()  >
                                    <#list   page.getList() as type>
                                        <tr>
                                            <td class="center aligned">
                                                ${type.id}
                                            </td>
                                            <td class="center aligned">
                                                ${type.type}
                                            </td>
                                            <td class="center aligned">
                                                <a href="${base}/manager/modifyType/${type.id}"
                                                   class="ui button primary">修改</a>
                                                <a href="${base}/manager/deleteType/${type.id}"
                                                   class="ui button primary">删除</a>

                                            </td>
                                        </tr>

                                    </#list>
                                <#else >

                                    <tr>
                                        <td class="fluid">
                                            <div class="ui negative massive message">
                                                <div class="header">
                                                    未查询到相关信息
                                                </div>
                                            </div>
                                        </td>
                                    </tr>

                                </#if>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="5">
                            <div class="ui right floated pagination menu">
                                <#if page.getPageNumber() !=1 >
                                    <a class="icon item" href="?page=${page.getPageNumber()-1}">
                                        <i class="left chevron icon"></i>
                                    </a>
                                </#if>
                                <#list  1..(page.getTotalPage()) as count >
                                    <a class="item">
                                        <#if page.getPageNumber()==count>
                                            <b>${count}</b>
                                        <#else>
                                            ${count}
                                        </#if>
                                    </a>
                                </#list>
                                <#if page.getPageNumber() != page.getTotalPage() >
                                    <a class="icon item" href="?page=${page.getPageNumber()+1}">
                                        <i class="right chevron icon"></i>
                                    </a>
                                </#if>
                            </div>
                        </th>
                    </tr>
                    </tfoot>
                </table>
            </td>

            <td class="two wide"></td>
        </tr>
        </tbody>
    </table>





</@override>
<@extends name="managerLayout.ftl"></@extends>