<!doctype html>
<html lang="zh">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <script src="${base}/scripts/jquery.min.js"></script>
    <script src="${base}/scripts/semantic.min.js"></script>
    <link rel="stylesheet" href="${base}/styles/semantic.min.css"/>

    <title><@block name="title"></@block></title>



</head>

<body>


<div class="ui grid ">
    <div class="row">
        <img class="ui  image  centered medium" src="${base}/template/image/title.jpg" title="" style="height: 150px">
    </div>
</div>

<div class="ui attached stackable menu">
    <div class="ui container">
        <div class="header item">
            美发工作室
        </div>

        <div class=" ui dropdown item">
            顾客信息管理
            <div class="menu">
                <a class="item" href="${base}/manager/listCustomer">查看顾客信息</a>
                <a class="item" href="${base}/manager/addCustomer">添加顾客信息</a>
            </div>
        </div>
        <div class=" ui dropdown item">
            员工信息管理
            <div class="menu">
                <a class="item" href="${base}/manager/listStaff">查看员工信息</a>
                <a class="item" href="${base}/manager/addStaff">添加员工信息</a>
            </div>
        </div>
        <div class=" ui dropdown item">
            理发类型管理
            <div class="menu">
                <a class="item" href="${base}/manager/listType">查看类型</a>
                <a class="item" href="${base}/manager/addType">添加类型</a>
            </div>
        </div>
        <a class="ui  item" href="${base}/manager/listHaircutTask">理发管理</a>

        <div class=" ui dropdown item">
            充值管理
            <div class="menu">
                <a class="item" href="${base}/manager/recharge">充值</a>
                <a class="item" href="${base}/manager/listRecharge">查看金额</a>
            </div>
        </div>


        <div class="right menu">


            <#if (session.user)??>
                <div class="ui dropdown item">
                    <i class="user icon"></i>
                    <div class="text"> 欢迎 ${session.user.name} 用户</div>
                    <i class="icon dropdown"></i>
                    <div class="menu">
                        <a href="${base}/logout" class="item">退出</a>
                    </div>
                </div>
            <#else>

                <div class="ui buttons">
                    <a class="ui button" href="${base}/login">登陆</a>
                    <div class="or"></div>
                    <a class="ui positive button" href="${base}/register">顾客注册</a>
                </div>
            </#if>
        </div>
    </div>
</div>


<@block name="content">

</@block>

<div class="ui footer menu bottom fixed  ">



</div>
</body>

<script>
    $('.ui.dropdown').dropdown();
</script>

</html>