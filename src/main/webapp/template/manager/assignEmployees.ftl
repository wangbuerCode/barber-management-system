<@override name="title">分配理发员工</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="four wide "></td>
            <td class="eight wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <thead>
                        <tr>
                            <th class=" center aligned  " colspan="4">
                                下列为空闲员工
                            </th>
                        </tr>
                        </thead>
                        <tbody>

                        <tr>
                            <td class="four wide center aligned "><b>请分配员工</b></td>
                            <td class="four wide  center aligned">
                                <div class="field ">
                                    <select class="ui dropdown  " name="staffId">
                                        <#list page as record>
                                            <option value="${record.id}">${record.name}：${record.skill}</option>
                                        </#list>
                                    </select>
                                    <input type="hidden" value="${taskId}" name="taskId">
                                </div>
                            </td>


                            <td>
                                <div class="ui fluid large teal submit button ">保存</div>
                            </td>
                        </tr>


                        </tbody>
                    </table>
                </form>
            </td>
            <td class="four wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                staffId: {
                    rules: [{
                        type: 'empty',
                        prompt: '不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/manager/doAssignEmployees',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/manager/listHaircutTask'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>