<#-- @ftlvariable name="recruit" type="model.Recruit" -->

<@override name="title">修改类型</@override>
<@override name="content">
    <table class="ui table celled padded  ">
        <tbody>
        <tr>

            <td class="three wide "></td>
            <td class="ten wide ">
                <form class="ui large form" id="apply-form">
                    <table class="ui celled striped  table">
                        <tbody>

                        <tr>
                            <td class="two wide center aligned "><b>ID</b></td>
                            <td class="four wide  center aligned disabled">
                                <div class="field">
                                    <input type="text" name="id" value="${type.id}" placeholder="${type.id}">
                                </div>
                            </td>


                            <td class="two wide center aligned "><b>类型名字</b></td>
                            <td class="four wide  center aligned">
                                <div class="field">
                                    <input type="text" name="type" value="${type.type}" placeholder="${type.type}">
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">
                                <div class="ui error message"></div>
                            </td>

                            <td>
                                <div class="ui fluid large teal submit button ">保存</div>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </form>
            </td>
            <td class="three wide"></td>
        </tr>
        </tbody>
    </table>

    <script>


        $('.ui.dropdown').dropdown();

        $('.ui.form').form({
            fields: {
                type: {
                    rules: [{
                        type: 'empty',
                        prompt: '类型信息不能为空'
                    }]
                }
            }
        }).api({
            method: 'POST',
            url: '${base}/manager/doModifyType',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    alert(res.message);
                    window.location.href = '${base}/manager/listType'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        })

    </script>

</@override>
<@extends name="managerLayout.ftl"></@extends>