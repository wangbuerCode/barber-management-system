<@override name="title">客户信息</@override>
<@override name="content">



	<table class="ui table celled padded  ">
		<tbody>
		<tr>
			<td class="two wide "></td>
			<td class="eleven wide ">
				<table class="ui red table equal width celled padded ">
					<thead>
					<tr>
						<th class=" center aligned  " colspan="5">
							搜索
						</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							<form class="ui fluid form">
								<table class="ui celled table">
									<tbody>
									<tr>
										<td>
											<div class="filed">
												<div class="ui labeled input">
													<div class="ui label">
														姓名
													</div>
													<input type="text" placeholder="请输入姓名" name="name">
												</div>
											</div>
										</td>
										<td>
											<div class="filed">
												<div class="ui labeled input ">
													<div class="ui label">
														电话号码
													</div>
													<input  type="text" placeholder="请输入电话号码" name="tel">
												</div>
											</div>
										</td>
										<td class="center aligned">
											<a class="ui primary submit button ">搜索</a><br>
										</td>
									</tr>
									<tr>
										<div class="ui error message"></div>
									</tr>

									</tbody>
								</table>
							</form>
						</td>
					</tr>
					<tr>
						<td>
							<table class="ui celled table">
								<thead>
								<tr>
									<th class="center aligned">ID</th>
									<th class="center aligned">姓名</th>
									<th class="center aligned">性别</th>
									<th class="center aligned">电话</th>
									<th class="center aligned">邮件地址</th>
									<th class="center aligned">地址</th>
									<th class="center aligned">密码</th>
									<th class="center aligned">操作</th>
								</tr>
								</thead>
								<tbody>
								<#if  !  page.getList().isEmpty()  >
									<#list   page.getList() as customer>
										<tr>
											<td class="center aligned">
												${customer.id}
											</td>
											<td class="center aligned">
												${customer.name}
											</td>
											<td class="center aligned">
												${customer.sex}
											</td>
											<td class="center aligned">
												${customer.tel}
											</td>
											<td class="center aligned">
												${customer.email}
											</td>
											<td class="center aligned">
												${customer.address}
											</td>
											<td class="center aligned">
												${customer.password}
											</td>
											<td class="center aligned">
												<a href="${base}/manager/modifyCustomer/${customer.id}"
												   class="ui button primary">修改</a>
												<a href="${base}/manager/deleteCustomer/${customer.id}"
												   class="ui button primary">删除</a>

											</td>
										</tr>

									</#list>
								<#else >

									<tr>
										<td class="fluid">
											<div class="ui negative massive message">
												<div class="header">
													未查询到相关信息
												</div>
											</div>
										</td>
									</tr>

								</#if>
								</tbody>
							</table>
						</td>
					</tr>
					</tbody>
					<tfoot>
					<tr>
						<th colspan="5">
							<div class="ui right floated pagination menu">
								<#if page.getPageNumber() !=1 >
									<a class="icon item" href="?page=${page.getPageNumber()-1}">
										<i class="left chevron icon"></i>
									</a>
								</#if>
								<#list  1..(page.getTotalPage()) as count >
									<a class="item">
										<#if page.getPageNumber()==count>
											<b>${count}</b>
										<#else>
											${count}
										</#if>
									</a>
								</#list>
								<#if page.getPageNumber() != page.getTotalPage() >
									<a class="icon item" href="?page=${page.getPageNumber()+1}">
										<i class="right chevron icon"></i>
									</a>
								</#if>
							</div>
						</th>
					</tr>
					</tfoot>
				</table>
			</td>

			<td class="two wide"></td>
		</tr>
		</tbody>
	</table>




	<script>
        $('.ui.form').form({
            fields: {}
        }).api({
            method: 'POST',
            url: '${base}/manager/queryCustomer',
            serializeForm: true,
            success: function (res) {
                if (res.success) {
                    window.location.href = '${base}/manager/queryCustomerResult'
                } else {
                    $('.ui.form').form('add errors', [res.message]);
                }
            }
        });
	</script>

</@override>
<@extends name="managerLayout.ftl"></@extends>